package com.java_test.customerandsims;

import com.java_test.customerandsims.domain.Customer;
import com.java_test.customerandsims.domain.Sim;
import com.java_test.customerandsims.repository.CustomerRepository;
import com.java_test.customerandsims.repository.SimRepository;
import com.java_test.customerandsims.service.CustomerService;
import com.java_test.customerandsims.service.SimService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class SimTest {

    @InjectMocks
    @Autowired
    private SimService simService;
    @MockBean
    private SimRepository simRepository;

    //testing for saving a new  sim
    @Test
    void save() {

        Sim sim = new Sim();
        sim.setId((long) 1);
        sim.setName("JAZZ");
        sim.setNumber("03085139071");

        Mockito.when(simRepository.save(sim)).thenReturn(sim);
        assertThat(simService.save(sim)).isEqualTo(sim);
    }

    //testing for getting a list of all sims
    @Test
    void findAll() {

        Sim sim = new Sim();
        sim.setId((long) 1);
        sim.setName("JAZZ");
        sim.setNumber("03085139071");

        Sim sim1 = new Sim();
        sim1.setId((long) 2);
        sim1.setName("JAZZ");
        sim1.setNumber("03085139072");


        List<Sim> simList = new ArrayList<>();
        simList.add(sim);
        simList.add(sim1);

        Mockito.when(simRepository.findAll()).thenReturn(simList);
        assertThat(simService.findAll()).isEqualTo(simList);

    }

    //testing for removing a sim by id
    @Test
    void delete() {
        Sim sim = new Sim();
        sim.setId((long) 1);
        sim.setName("JAZZ");
        sim.setNumber("03085139071");

        Mockito.when(simRepository.save(sim)).thenReturn(sim);
        boolean present1 = simRepository.findById((long) 1).isPresent();
        simRepository.deleteById((long) 1);
        boolean present2 = simRepository.findById((long) 1).isPresent();
        assertThat(present1).isEqualTo(present2);
    }



}
