package com.java_test.customerandsims;


import com.java_test.customerandsims.domain.Customer;
import com.java_test.customerandsims.domain.Sim;
import com.java_test.customerandsims.repository.CustomerRepository;
import com.java_test.customerandsims.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CustomerTest {

    @InjectMocks
    @Autowired
    private CustomerService customerService;
    @MockBean
    private CustomerRepository customerRepository;

//testing for saving a new customer
    @Test
    void save() {

        Customer customer = new Customer();
        customer.setId((long) 1);
        customer.setFirstName("salman");
        customer.setLastName("Mehboob");
        customer.setMiddleName("");
        customer.setGender("Male");
        customer.setPrimaryContactNumber("03085139071");
        customer.setCnic("37237137813691");
        customer.setAddress("house 737");
        customer.setCity("Multan");
        customer.setDOB("java.time.LocalDate.now()");
        customer.setPersonalEmail("salmanmehboob60@gmail.com");
        customer.setDataCreated(LocalDate.now());
        Sim sim = new Sim();
        sim.setId((long) 1);
        customer.setSim(sim);

        Mockito.when(customerRepository.save(customer)).thenReturn(customer);
        assertThat(customerService.save(customer)).isEqualTo(customer);
    }

    //testing for getting a list of all customers
        @Test
    void findAll() {

            Customer customer = new Customer();
            customer.setId((long) 1);
            customer.setFirstName("salman");
            customer.setLastName("Mehboob");
            customer.setMiddleName("");
            customer.setGender("Male");
            customer.setPrimaryContactNumber("03085139071");
            customer.setCnic("37237137813691");
            customer.setAddress("house 737");
            customer.setCity("Multan");
            customer.setDOB("java.time.LocalDate.now()");
            customer.setPersonalEmail("salmanmehboob60@gmail.com");
            customer.setDataCreated(LocalDate.now());
            Sim sim = new Sim();
            sim.setId((long) 1);
            customer.setSim(sim);

            Customer customer1 = new Customer();
            customer1.setId((long) 2);
            customer1.setFirstName("Nisa");
            customer1.setLastName("salman");
            customer1.setGender("female");
            customer1.setPrimaryContactNumber("03085139071");
            customer1.setCnic("37237137813691");
            customer1.setAddress("house 737");
            customer1.setCity("Multan");
            customer1.setDOB("java.time.LocalDate.now()");
            customer1.setPersonalEmail("salmanmehboob60@gmail.com");
            customer1.setDataCreated(LocalDate.now());
            Sim sim1= new Sim();
            sim1.setId((long) 2);
            customer.setSim(sim1);


        List<Customer> customerList = new ArrayList<>();
            customerList.add(customer);
            customerList.add(customer1);

        Mockito.when(customerRepository.findAll()).thenReturn(customerList);
        assertThat(customerService.findAll()).isEqualTo(customerList);

    }
//testing for removing a customer by id
        @Test
    void delete() {
            Customer customer = new Customer();
            customer.setId((long) 1);
            customer.setFirstName("salman");
            customer.setLastName("Mehboob");
            customer.setMiddleName("");
            customer.setGender("Male");
            customer.setPrimaryContactNumber("03085139071");
            customer.setCnic("37237137813691");
            customer.setAddress("house 737");
            customer.setCity("Multan");
            customer.setDOB("java.time.LocalDate.now()");
            customer.setPersonalEmail("salmanmehboob60@gmail.com");
            customer.setDataCreated(LocalDate.now());
            Sim sim = new Sim();
            sim.setId((long) 1);
            customer.setSim(sim);

        Mockito.when(customerRepository.save(customer)).thenReturn(customer);
        boolean present1 = customerRepository.findById((long) 1).isPresent();
        customerRepository.deleteById((long) 1);
        boolean present2 = customerRepository.findById((long) 1).isPresent();
        assertThat(present1).isEqualTo(present2);
    }

}
