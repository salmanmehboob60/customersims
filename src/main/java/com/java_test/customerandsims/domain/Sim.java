package com.java_test.customerandsims.domain;


import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Data
@Table(name="sim")
public class Sim {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Size(max = 50)
    @Column(name = "type", length = 50, nullable = false)
    private String type;

    @Size(max = 50)
    @Column(name = "number", length = 50, nullable = false)
    private String number;


}
