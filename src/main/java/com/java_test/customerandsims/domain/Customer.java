package com.java_test.customerandsims.domain;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Entity;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Data
@Table(name="customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    @Column(name = "first_name", length = 50, nullable = false)
    private String firstName;

    @Size(max = 50)
    @Column(name = "middle_name", length = 50)
    private String middleName;


    @Size(max = 50)
    @Column(name = "last_name", length = 50, nullable = false)
    private String lastName;


    @Size(max = 50)
    @Column(name = "gender", length = 50, nullable = false)
    private String gender;


    @Size(max = 15)
    @Column(name = "contactNumber", length = 150, nullable = false)
    private String primaryContactNumber;


    @Column(name = "cnic", length = 100, nullable = false)
    private String cnic;


    @Size(max = 200)
    @Column(name = "address", length = 200, nullable = false)
    private String address;

    @Size(max = 100)
    @Column(name = "city", length = 100, nullable = false)
    private String city;

    @Column(name = "DOB")
    private String DOB;

    @Column(name = "personalEmail", length = 250)
    private String personalEmail;

    @Column(name = "dateCreated", length = 250)
    private LocalDate dataCreated;

    @ManyToOne
    @JsonIgnoreProperties(value = "customer", allowSetters = true)
    private Sim sim;

}
