package com.java_test.customerandsims;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class CustomerandsimsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomerandsimsApplication.class, args);
    }

}
