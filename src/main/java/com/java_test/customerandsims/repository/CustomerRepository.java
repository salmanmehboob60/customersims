package com.java_test.customerandsims.repository;

import com.java_test.customerandsims.domain.Customer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Customer findOneById(Long id);
}
