package com.java_test.customerandsims.repository;

import com.java_test.customerandsims.domain.Sim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SimRepository extends JpaRepository<Sim, Long> {
}
