package com.java_test.customerandsims.controller;

import com.java_test.customerandsims.domain.Customer;
import com.java_test.customerandsims.domain.Sim;
import com.java_test.customerandsims.dto.CustomerDTO;
import com.java_test.customerandsims.repository.CustomerRepository;
import com.java_test.customerandsims.repository.SimRepository;
import com.java_test.customerandsims.service.CustomerService;
import com.java_test.customerandsims.service.MailService;
import com.java_test.customerandsims.service.SimService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/api")
public class MainController {

    @Autowired
    private SimService simService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private SimRepository simRepository;
    @Autowired
    private MailService mailService;


//saving new sim
    @PostMapping(value = "/saveSim")
    public ResponseEntity<Sim> saveSim(@Validated Sim sim) {
        Sim simSaved = simService.save(sim);
        return ResponseEntity.ok(simSaved);
    }
    //saving new customer
    @PostMapping(value = "/saveCustomer")
    public ResponseEntity<Customer> saveCustomer(@Validated Customer customer) {
        Customer customerSaved = customerService.save(customer);
        return ResponseEntity.ok(customerSaved);
    }


//getting all customers
    @GetMapping("/getAllCustomers")
    public ResponseEntity<?> findAllCustomers() {
        List<Customer> customerList = customerRepository.findAll();

        if (customerList.isEmpty()) {
            return ResponseEntity.ok("Found no customer!");
        } else {
            return ResponseEntity.ok(customerList);
        }
    }
//getting all sims
    @GetMapping("/getAllSims")
    public ResponseEntity<?> findAllSim() {
        List<Sim> simList = simRepository.findAll();
        if (simList.isEmpty()) {
            return ResponseEntity.ok("Found no sim!");
        } else {
            return ResponseEntity.ok(simList);
        }
    }


    //this will run every 24hr = 86400000 sec
    @Scheduled(fixedRate = 86400000, initialDelay = 3000000)
    public void checkingBirthday() throws Exception {
        HttpServletResponse response = null;
        //this line of code would get a list of all email of customers you have birthday right after 7 days.
        String dobEmailsSevenDayBefore = customerRepository.findAll()
                .stream()
                .filter(s -> !s.getDOB().equals( LocalDateTime.now().minusDays(7).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .map(Customer::getPersonalEmail)
                .collect(Collectors.joining(","));

        String[] emailArray = dobEmailsSevenDayBefore.split(",");


        for (String a : emailArray) {

                //String senderName = userList.getEmployee().getFirstName();
                String toEmail = a;
                String subject = "BirthDay Alert!!!";
                String text = "Dear Customer, It's an email to notify you about your birthday in coming week. Have a good day ahead.";
//                   log.info("Sending email for request " + e.getEmployee().getFirstName());
                mailService.sendEmail(toEmail, subject, text);


        }
//this code will made a list of numbers whos birthday is today
        String dobTodayNumbers = customerRepository.findAll()
                .stream()
                .filter(s -> !s.getDOB().equals( LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .map(s -> s.getSim().getNumber())
                .collect(Collectors.joining(","));

        String[] numbersArray = dobTodayNumbers.split(",");

        String currentDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=customers_" + currentDateTime + ".csv";
        response.setHeader(headerKey, headerValue);

        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
        String[] csvHeader = {"Customer Numbers"};
        String[] nameMapping = {"number"};

        csvWriter.writeHeader(csvHeader);

        for (String i : numbersArray) {
            csvWriter.write(i, nameMapping);
        }

        csvWriter.close();


   }
}
