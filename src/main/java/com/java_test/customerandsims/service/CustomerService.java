package com.java_test.customerandsims.service;

import com.java_test.customerandsims.domain.Customer;
import com.java_test.customerandsims.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    public Customer save(Customer customer) {
        log.debug("Request to save customer : {}", customer);
        return customerRepository.save(customer);
    }

    public List<Customer> findAll() {
        log.debug("Request to get all user");
        return customerRepository.findAll();

    }

    public void delete(Long id) {
        log.debug("Request to delete customer : {}", id);
        customerRepository.deleteById(id);
    }


}

