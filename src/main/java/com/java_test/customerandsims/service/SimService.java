package com.java_test.customerandsims.service;

import com.java_test.customerandsims.domain.Customer;
import com.java_test.customerandsims.domain.Sim;
import com.java_test.customerandsims.repository.CustomerRepository;
import com.java_test.customerandsims.repository.SimRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class SimService {

    private SimRepository simRepository;

    public Sim save(Sim sim) {
        log.debug("Request to save sim : {}", sim);
        return simRepository.save(sim);
    }

    public List<Sim> findAll() {
        log.debug("Request to get all sim");
        return simRepository.findAll();

    }

    public void delete(Long id) {
        log.debug("Request to delete sim : {}", id);
        simRepository.deleteById(id);
    }

}
