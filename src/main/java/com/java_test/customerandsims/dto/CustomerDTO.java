package com.java_test.customerandsims.dto;

import lombok.Data;

@Data
public class CustomerDTO {
    Long customerID;
    Long simId;
}
